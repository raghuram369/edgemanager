package helm

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var namespace = "default"

const urlDeployedReleases = "http://192.168.10.152:31316/tiller/v2/releases/json"

//const urlDeployedReleases = "http://192.168.0.104:30065/tiller/v2/releases/json"

//const urlRelease = "http://192.168.0.104:30065/tiller/v2/releases/"

const urlRelease = "http://192.168.10.152:31316/tiller/v2/releases/"

//const ipRelease = "http://192.168.0.104:8879/charts/"

const ipRelease = "http://192.168.10.100:8879/charts/"
const token = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6Im15LWRhc2hib2FyZC1zYS10b2tlbi04ZzV2NCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJteS1kYXNoYm9hcmQtc2EiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJlOTMwMmQ3Ni0zMWNhLTExZTgtOTg0Zi1lY2IxZDczNGNhYTkiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGVmYXVsdDpteS1kYXNoYm9hcmQtc2EifQ.N3TWAbIrB5FiC83_KWPii1twgLhOHA8ca6E6UMFotDbGPqM99ECVoYIGU6wNwxZMj9j8ULkOtqYW9ql7nMLosTVpw4ooKB_zxdmXq0kdaDOAQJtj60NI7n-Lnc9Cd4QyFC2XI2AZchHVsE_hUH-j2Lb0d6fk2QUIiVisHWZV4x95_I_hwkNH2NKhcSuIVKDgiNucI592dKtWwKgOZLfh31Uh2OdA4jeSmFmzXwFFhykFlCN_q8THk5CxUc1TatqOeR6LL8t77bbGDNiF2xyJlLtpyo-4-yabQypsm7HzTRXkITsaSbZ9e5Qlo2xdYgN1_VzzfxmbsGpSj6V0U7a5Sg"

//const token = "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4tOHZieDIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjFhODJkYzU0LTQ1NzQtMTFlOC04YzY1LTA4MDAyNzEyMWFkZiIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.q1_GGVM_fZSBIW327qxBKTcLtelOMv9aXxkxySWcNV9QhbScacFBvOviHYXX4QDNDwFrcuXPwwC8NUQ21uJBm_b7GwEZNtiwbPS0aKthPOODBFEmRDzIUxfpvRM1XLA9nmYkew69HP9eTy8F9OIZggi5wClWVUCnG2x7Oogw7dCpgXWEZ5n1hGurGYv49xyioukLb605ozbq-GpNPY8ew1Yjwyuaa0gYRmxHu2FTg9VbaI22Mjo9BBV4V83so7hExqX_btD0Bgv17EBUmq8uP7O0QMMynwBYv69o2nEVprAfTzhG1IY-pDOv8A0SxJ9YQ1gyM0QvpnFu-7RkISRa4g"

//Release struct for helm chart
type Release struct {
	Releases []struct {
		Name string `json:"name"`
		Info struct {
			Status struct {
				Code  string `json:"code"`
				Notes string `json:"notes"`
			} `json:"status"`
			FirstDeployed time.Time `json:"first_deployed"`
			LastDeployed  time.Time `json:"last_deployed"`
			Description   string    `json:"Description"`
		} `json:"info"`
		ChartMetadata struct {
			Name        string   `json:"name"`
			Home        string   `json:"home"`
			Sources     []string `json:"sources"`
			Version     string   `json:"version"`
			Description string   `json:"description"`
			Maintainers []struct {
				Name  string `json:"name"`
				Email string `json:"email"`
			} `json:"maintainers"`
			Icon       string `json:"icon"`
			APIVersion string `json:"apiVersion"`
			AppVersion string `json:"appVersion"`
		} `json:"chart_metadata"`
		Config struct {
			Raw string `json:"raw"`
		} `json:"config"`
		Version   int    `json:"version"`
		Namespace string `json:"namespace"`
	} `json:"releases"`
}

type chart struct {
	ChartURL string `json:"chart_url"`
}

//GetDeployedReleases gets the list of deployed helm charts
func GetDeployedReleases() (rel Release, err error) {
	swiftClient := http.Client{Timeout: time.Second * 180}
	releaseObj := Release{}

	req, err := http.NewRequest(http.MethodGet, urlDeployedReleases, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", token)

	res, getErr := swiftClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return releaseObj, getErr

	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return releaseObj, readErr

	} else {
		jsonErr := json.Unmarshal(body, &releaseObj)
		if jsonErr != nil {
			log.Fatal(jsonErr)
			return releaseObj, jsonErr
		}
		/* for i := 0; i < len(releaseObj.Releases); i++ {

		releaseIndividual := releaseObj.Releases[i]
		fmt.Println(releaseIndividual.Name)
		fmt.Println(releaseIndividual.Info.Description)
		fmt.Println(releaseIndividual.Info.Status.Code)
		fmt.Println(releaseIndividual.Info.FirstDeployed)
		fmt.Println(releaseIndividual.Info.LastDeployed)
		fmt.Println(releaseIndividual.ChartMetadata.Version)*/
		return releaseObj, nil
	}
}

//DeployRelease deploys a new helm chart to the cluster based on the name and version specified
func DeployRelease(releaseName string, releaseVersion string) (response string, err error) {

	swiftClient := http.Client{Timeout: time.Second * 180}
	chartObj := chart{}
	chartObj.ChartURL = ipRelease + releaseName + "-" + releaseVersion + ".tgz"
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(chartObj)
	log.Println(b)
	log.Println(urlRelease + releaseName + "/json")
	req, err := http.NewRequest(http.MethodPost, urlRelease+releaseName+"/json", b)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	res, getErr := swiftClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return "Failed to deploy helm chart", getErr

	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return "Failed to deploy helm chart", readErr

	}

	log.Println(string(body))
	return "Helm chart " + releaseName + " is successfully deployed", nil

}

//UnDeployRelease removes an existing helm chart from the cluster based on the name  specified
func UnDeployRelease(releaseName string) (response string, err error) {

	swiftClient := http.Client{}
	req, err := http.NewRequest(http.MethodDelete, urlRelease+releaseName+"/json", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", token)

	res, getErr := swiftClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return "Failed to undeploy helm chart", getErr

	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return "Failed to undeploy helm chart", readErr

	}
	log.Println(string(body))

	return "Helm chart " + releaseName + " is successfully undeployed", nil

}

//PurgeRelease deletes the helm chart from the cluster based on the name specified
func PurgeRelease(releaseName string) (response string, err error) {

	swiftClient := http.Client{Timeout: time.Second * 180}
	req, err := http.NewRequest(http.MethodDelete, urlRelease+releaseName+"/json?purge=true", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", token)

	res, getErr := swiftClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return "Failed to purge helm chart", getErr

	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return "Failed to purge helm chart", readErr

	}
	log.Println(string(body))

	return "Helm chart " + releaseName + " is successfully purged", nil

}

//UpdateRelease updates a  helm chart in the cluster based on the name and version specified
func UpdateRelease(releaseName string, releaseVersion string) (response string, err error) {

	swiftClient := http.Client{Timeout: time.Second * 180}
	chartObj := chart{}
	chartObj.ChartURL = ipRelease + releaseName + "-" + releaseVersion + ".tgz"
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(chartObj)
	req, err := http.NewRequest(http.MethodPut, urlRelease+releaseName+"/json", b)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	res, getErr := swiftClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return "Failed to update helm chart", getErr

	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return "Failed to update helm chart", readErr

	}
	log.Println(string(body))

	return "Helm chart " + releaseName + " is successfully updated to version " + releaseVersion, nil

}

//RollBackRelease rolls back a helm chart in the cluster based on the name specified
func RollBackRelease(releaseName string) (response string, err error) {

	swiftClient := http.Client{Timeout: time.Second * 180}
	req, err := http.NewRequest(http.MethodGet, urlRelease+releaseName+"/rollback/json", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", token)

	res, getErr := swiftClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return "Failed to rollback helm chart", getErr

	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return "Failed to rollback helm chart", readErr

	}
	log.Println(string(body))

	return "Helm chart " + releaseName + " is successfully rolled back to previous version ", nil

}
