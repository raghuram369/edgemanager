package main

import (
	"log"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	pb "github.com/raghu/EdgeManager/manager"
)

const (
	address = "localhost:50051"
)

// ActivateHelmChart calls the RPC method ActivateHelmChart of helm manager
func ActivateHelmChart(client pb.ManagerClient, helm *pb.HelmActivateRequest) {
	resp, err := client.ActivateHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not activate helm chart: %v", err)
	}
	if resp.Success {
		log.Printf("Helm chart is successfully deployed")
	}
}

// DeactivateHelmChart calls the RPC method DeactivateHelmChart of helm manager
func DeactivateHelmChart(client pb.ManagerClient, helm *pb.HelmRequest) {
	resp, err := client.DectivateHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not deactivate helm chart: %v", err)
	}
	if resp.Success {
		log.Printf("Helm chart is successfully un deployed")
	}
}

// RollbackHelmChart calls the RPC method RollbackHelmChart of helm manager
func RollbackHelmChart(client pb.ManagerClient, helm *pb.HelmRequest) {
	resp, err := client.RollbackHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not rollback helm chart: %v", err)
	}
	if resp.Success {
		log.Printf("Helm chart is successfully rolled back")
	}
}

// UpdateHelmChart calls the RPC method UpdateHelmChart of helm manager
func UpdateHelmChart(client pb.ManagerClient, helm *pb.HelmActivateRequest) {
	resp, err := client.UpdateHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not update helm chart: %v", err)
	}
	if resp.Success {
		log.Printf("Helm chart is successfully updated")
	}
}

// PurgeHelmChart calls the RPC method DeactivateHelmChart of helm manager
func PurgeHelmChart(client pb.ManagerClient, helm *pb.HelmRequest) {
	resp, err := client.PurgeHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not purgee helm chart: %v", err)
	}
	if resp.Success {
		log.Printf("Helm chart is successfully purged")
	}
}

// GetListActiveHelmCharts calls the RPC method ListActiveHelmCharts of CustomerServer
func GetListActiveHelmCharts(client pb.ManagerClient, filter *pb.HelmFilter) {
	// calling the streaming API
	HelmListResponse, err := client.ListActiveHelmCharts(context.Background(), filter)
	if err != nil {
		log.Fatalf("Error on get list of active helm charts: %v", err)
	}

	log.Printf("Helm list: %v", HelmListResponse.ReleaseList)

}
func main() {
	// Set up a connection to the gRPC server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	// Creates a new CustomerClient
	client := pb.NewManagerClient(conn)

	helmRequest := &pb.HelmRequest{
		Name: "video_analytics",
	}

	// activate a new helm chart
	activateHelmChart(client, helmRequest)

	helmRequestNew := &pb.HelmRequest{
		Name: "VR_app",
	}

	activateHelmChart(client, helmRequestNew)
	// Filter with an empty Keyword
	filter := &pb.HelmFilter{Keyword: ""}
	getListActiveHelmCharts(client, filter)
	deactivateHelmChart(client, helmRequestNew)
	getListActiveHelmCharts(client, filter)

}
