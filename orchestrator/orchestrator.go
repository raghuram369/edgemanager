package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/raghu/EdgeManager/new"

	"golang.org/x/crypto/ssh"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type Service struct {
	Name string `json:"name,omitempty"`
	ID   string `json:"id,omitempty"`
}

func analyticsStart(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	ID := params["id"]
	dname := params["name"]
	if ID == "analytics" {

		cmd := "echo ubuntu |  sudo -S docker-compose -f /home/ubuntu/Documents/docker-compose.yaml -p " + dname + " up -d"

		output, err := remoteRun("ubuntu", "192.168.10.154", "ubuntu", cmd)

		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(output)
		outputconsul, errconsul := new.Test()

		if errconsul != nil {
			fmt.Println(errconsul)

		}

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(outputconsul))
	}

}

func analyticsStop(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	ID := params["id"]
	dname := params["name"]
	if ID == "analytics" {

		cmd := "echo ubuntu |  sudo -S docker kill " + dname + "_machinery_1 " + dname + "_web_1"

		output, err := remoteRun("ubuntu", "192.168.10.154", "ubuntu", cmd)

		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(output)

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("Service stopped"))
	}

}
func main() {
	router := mux.NewRouter()
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})
	originsOk := handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED"), "http://localhost:9090", "http://192.168.10.133:9090"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	router.HandleFunc("/service/start/{id}/{name}", analyticsStart).Methods("POST")
	router.HandleFunc("/service/stop/{id}/{name}", analyticsStop).Methods("POST")
	log.Fatal(http.ListenAndServe(":12345", handlers.CORS(originsOk, headersOk, methodsOk)(router)))

}
func remoteRun(user string, addr string, password string, cmd string) (string, error) {
	config := &ssh.ClientConfig{
		User: user,

		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	client, err := ssh.Dial("tcp", addr+":22", config)
	if err != nil {
		return "", err
	}
	session, err := client.NewSession()
	if err != nil {
		return "", err
	}
	defer session.Close()
	var b bytes.Buffer
	session.Stdout = &b
	err = session.Run(cmd)
	return b.String(), err
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
