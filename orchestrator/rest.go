package main

//DeployToSite struct
type DeployToSite struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	Sites   []struct {
		Name string `json:"name"`
	} `json:"sites"`
}

//ActivateHelm struct for activate and updating helm charts
type ActivateHelm struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

//DeactivateHelm struct for deactivating , rollback and purging helm charts
type DeactivateHelm struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}
