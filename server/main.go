package main

import (
	"encoding/json"
	"log"
	"net"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	"github.com/raghu/EdgeManager/helm"
	pb "github.com/raghu/EdgeManager/manager"
)

const (
	port = ":50051"
)

type server struct {
}

// ActivateHelmChart activates a helm chart
func (s *server) ActivateHelmChart(ctx context.Context, in *pb.HelmActivateRequest) (*pb.HelmResponse, error) {

	_, err := helm.DeployRelease(in.Name, in.Version)
	if err != nil {
		return &pb.HelmResponse{Id: 1, Success: false}, err
	}
	return &pb.HelmResponse{Id: 1, Success: true}, nil
}

// DectivateHelmChart deactivates a give helm chart
func (s *server) DectivateHelmChart(ctx context.Context, in *pb.HelmRequest) (*pb.HelmResponse, error) {
	_, err := helm.UnDeployRelease(in.Name)
	if err != nil {
		return &pb.HelmResponse{Id: 1, Success: false}, err
	}
	return &pb.HelmResponse{Id: 1, Success: true}, nil
}

// PurgeHelmChart deletes a give helm chart
func (s *server) PurgeHelmChart(ctx context.Context, in *pb.HelmRequest) (*pb.HelmResponse, error) {
	_, err := helm.PurgeRelease(in.Name)
	if err != nil {
		return &pb.HelmResponse{Id: 1, Success: false}, err
	}
	return &pb.HelmResponse{Id: 1, Success: true}, nil
}

// UpdateHelmChart updates a give helm chart
func (s *server) UpdateHelmChart(ctx context.Context, in *pb.HelmActivateRequest) (*pb.HelmResponse, error) {
	_, err := helm.UpdateRelease(in.Name, in.Version)
	if err != nil {
		return &pb.HelmResponse{Id: 1, Success: false}, err
	}
	return &pb.HelmResponse{Id: 1, Success: true}, nil
}

// RollbackHelmChart rollsback a give helm chart
func (s *server) RollbackHelmChart(ctx context.Context, in *pb.HelmRequest) (*pb.HelmResponse, error) {
	_, err := helm.RollBackRelease(in.Name)
	if err != nil {
		return &pb.HelmResponse{Id: 1, Success: false}, err
	}
	return &pb.HelmResponse{Id: 1, Success: true}, nil
}

// ListActiveHelmCharts lists all active helm charts
func (s *server) ListActiveHelmCharts(ctx context.Context, filter *pb.HelmFilter) (*pb.HelmListResponse, error) {

	resp, err := helm.GetDeployedReleases()
	if err != nil {
		return &pb.HelmListResponse{Id: 1, ReleaseList: "none"}, err
	}
	response, err := json.Marshal(resp)
	if err != nil {
		return &pb.HelmListResponse{Id: 1, ReleaseList: "none"}, err
	}

	return &pb.HelmListResponse{Id: 1, ReleaseList: string(response)}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// Creates a new gRPC server
	s := grpc.NewServer()
	pb.RegisterManagerServer(s, &server{})
	s.Serve(lis)
}
