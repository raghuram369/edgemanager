package new

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

var myClient = &http.Client{Timeout: 10 * time.Second}

type Base struct {
	App      string `json:"app"`
	Services []Service
}

type Service struct {
	ID      string `json:"ID"`
	Service string `json:"Service"`
	Port    string `json:"Port"`
}

func getJson(url string) (map[string]interface{}, error) {
	r, err := myClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()
	var data map[string]interface{}
	body, err := ioutil.ReadAll(r.Body)
	errnew := json.Unmarshal(body, &data)
	return data, errnew
}

func Test() (string, error) {
	r, err := getJson("http://192.168.10.154:8500/v1/agent/services")
	if err != nil {

		fmt.Println(err)

	}
	b := &Base{}
	b.App = "analytics"
	b.Services = []Service{}

	keys := reflect.ValueOf(r).MapKeys()
	strkeys := make([]string, len(keys))
	for i := 0; i < len(keys); i++ {
		strkeys[i] = keys[i].String()
		str := keys[i].String()
		//service := &Service{}
		if !strings.Contains(str, "consul") {
			s := Service{Service: r[str].(map[string]interface{})["Service"].(string), ID: r[str].(map[string]interface{})["ID"].(string), Port: strconv.FormatFloat((r[str].(map[string]interface{})["Port"].(float64)), 'f', -1, 64)}

			b.Services = append(b.Services, s)
		}
	}
	out, err := json.Marshal(b)

	return string(out), err
}
